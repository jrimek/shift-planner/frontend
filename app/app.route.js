angular.module('shiftPlanner')

.config(function($routeProvider/*, $locationProvider*/){
  $routeProvider
  .when("/department/:dept_id/:week",
    {
      templateUrl: "app/components/department/departmentView.html",
      controller: "DepartmentController",
      controllerAs: "dept"
    })
  .otherwise({
    redirectTo: '/department/2/0'
  });

  //$locationProvider.html5Mode(true);
})
;