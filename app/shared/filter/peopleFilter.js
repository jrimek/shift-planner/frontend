angular
.module("shiftPlanner")
.filter('peopleFilter', function(){
  return function(people, filter_criteria){
    //add check for roles = null
    var roles = filter_criteria['roles'];
    var skills = filter_criteria['skills'];
    var languages = filter_criteria['languages'];
    var shifts = filter_criteria['shifts'];

    var filter_roles = [];
    var filter_skills = [];
    var filter_languages = [];
    var filter_shifts = [];

    var filtered_people = [];

    for (var i = 0; i < roles.length; i++){
      if (roles[i].filter === true){
        filter_roles.push(roles[i].id);
      }
    }

    for (var i = 0; i < skills.length; i++){
      if (skills[i].filter === true){
        filter_skills.push(skills[i].id);
      }
    }

    for (var i = 0; i < languages.length; i++){
      if (languages[i].filter === true){
        filter_languages.push(languages[i].id);
      }
    }

    for (var i = 0; i < shifts.length; i++){
      if (shifts[i].filter === true){
        filter_shifts.push(shifts[i].id);
      }
    }

    for (var i = 0; i < people.length; i++){
      var show_person = []; //best var name eu!!!
      if (filter_roles.length === 0){
        show_person[0] = true; 
      } else {
        if (people[i].roles[0] === null){
          show_person[0] = false; 
        } else {
          show_person[0] = false;
          for (var j = 0; j < people[i].roles.length; j++){
            if (filter_roles.indexOf(people[i].roles[j].id) >= 0){
              show_person[0] = true;
            }
          }
        }
      }

      if (filter_skills.length === 0){
        show_person[1] = true; 
      } else {
        if (people[i].skills[0] === null){
          show_person[1] = false; 
        } else {
          show_person[1] = false;
          for (var j = 0; j < people[i].skills.length; j++){
            if (filter_skills.indexOf(people[i].skills[j].id) >= 0){
              show_person[1] = true;
            }
          }
        }
      }

      if (filter_languages.length === 0){
        show_person[2] = true; 
      } else {
        if (people[i].languages[0] === null){
          show_person[2] = false; 
        } else {
          show_person[2] = false;
          for (var j = 0; j < people[i].languages.length; j++){
            if (filter_languages.indexOf(people[i].languages[j].id) >= 0){
              show_person[2] = true;
            }
          }
        }
      }

      if (filter_shifts.length === 0){
        show_person[3] = true; 
      } else {
        //loop through all shifts
        var show_shift = []
        for (var j = 0; j < people[i].shifts.length; j++){
          show_shift[j] = false;
          if (filter_shifts.indexOf(people[i].shifts[j].shift_type_id) >= 0){
            show_shift[j] = true;
          }  
        }

        if (show_shift.indexOf(true) >= 0){
          show_person[3] = true;
        } else {
          show_person[3] = false;
        }
      }
      
      if (typeof filter_criteria['person_name'] != 'undefined') {
        if (people[i].name.toLowerCase().indexOf(filter_criteria['person_name'].toLowerCase()) >= 0){
          show_person[4] = true;
        } else {
          show_person[4] = false;
        }
      }

      if (show_person.indexOf(false) === -1){
        filtered_people.push(people[i]);
      }
      
    }
    return filtered_people;
  }
});