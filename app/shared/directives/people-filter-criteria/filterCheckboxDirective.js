angular
.module('shiftPlanner')
.directive("filterCheckbox", function() {
  return {
    restrict: "E",
    scope: { type: '=', name: '=' },
    templateUrl: "app/shared/directives/people-filter-criteria/filterCheckboxView.html"
  }
});