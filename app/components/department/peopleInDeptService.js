function peopleInDeptService($resource, $routeParams){
  return $resource('http://localhost/people_in_dept/:dept_id/:week' + '.json', {dept_id: 2, week:0});
}

angular
.module("shiftPlanner")
.service("peopleInDeptService", peopleInDeptService)
;
