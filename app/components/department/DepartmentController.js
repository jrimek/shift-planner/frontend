function DepartmentController($routeParams, 
                              peopleInDeptService, 
                              departmentService){
  var self = this;

  self.people = peopleInDeptService.query({dept_id:$routeParams.dept_id, week:$routeParams.week});
  self.departments = departmentService.query();

  self.dept_id = $routeParams.dept_id;
  self.nextWeek = parseInt($routeParams.week) + 1;
  self.prevWeek = parseInt($routeParams.week) - 1;
  self.searchName = '';
  
  //----------------------days-calc------------------------
  //this was so much nicer in ruby...
  var date = new Date();
  var days_from_monday = date.getDay() - 1;
  var weeks_from_now = parseInt($routeParams.week)

  week = [
  new Date(), //monday
  new Date(), //etc
  new Date(),
  new Date(),
  new Date()
  ];

  week[0].setDate(date.getDate() + weeks_from_now * 7 - days_from_monday);
  week[1].setDate(date.getDate() + weeks_from_now * 7 - days_from_monday + 1);
  week[2].setDate(date.getDate() + weeks_from_now * 7 - days_from_monday + 2);
  week[3].setDate(date.getDate() + weeks_from_now * 7 - days_from_monday + 3);
  week[4].setDate(date.getDate() + weeks_from_now * 7 - days_from_monday + 4);

  self.monday = "Mon. " + week[0].getDate() + ". " + (parseInt(week[0].getMonth()) + 1) + ".";
  self.tuesday = "Tues. " + week[1].getDate() + ". " + (parseInt(week[1].getMonth()) + 1) + ".";
  self.wednsday = "Wed. " + week[2].getDate() + ". " + (parseInt(week[2].getMonth()) + 1) + ".";
  self.thursday = "Thurs. " + week[3].getDate() + ". " + (parseInt(week[3].getMonth()) + 1) + ".";
  self.friday = "Fri. " + week[4].getDate() + ". " + (parseInt(week[4].getMonth()) + 1) + ".";
  //self.monday = start_of_week
}

angular
.module("shiftPlanner")
.controller("DepartmentController", DepartmentController)
;
