function departmentService($resource, $routeParams){
  return $resource('http://localhost/departments' + '.json');
}

angular
.module("shiftPlanner")
.service("departmentService", departmentService)
;
