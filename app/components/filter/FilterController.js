function FilterController($routeParams, filterCriteriaService){
  var self = this;
  self.criteria = filterCriteriaService.get({dept_id:$routeParams.dept_id, week:$routeParams.week});
};

angular
.module("shiftPlanner")
.controller("FilterController", FilterController);