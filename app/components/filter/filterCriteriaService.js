function filterCriteriaService($resource){
  return $resource('http://localhost/filters/people/:dept_id/:week' + '.json', {dept_id: 2, week:0});
}

angular
.module("shiftPlanner")
.service("filterCriteriaService", filterCriteriaService);