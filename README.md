# README

this is the frontend part to my shiftplanner.
For more information pls checkout the main [repository](https://github.com/Elux91/shift-planner)

- [x] displaying people and their roles, skills and languages in a department
- [x] displaying people shifts in a department
- [x] show the next / previous week
- [x] switch between departments
- [x] filter the displayed people by role, skill, language, shift and name
- [ ] update shifts in the table
- [ ] create/delete shifts
  - [ ] create
  - [ ] delete
- [ ] create/update/delete departments
  - [ ] create
  - [ ] update
  - [ ] delete
- [ ] create/update/delete people
  - [ ] create
  - [ ] update
  - [ ] delete
- [ ] create/update/delete roles
  - [ ] create
  - [ ] update
  - [ ] delete
- [ ] create/update/delete skills
  - [ ] create
  - [ ] update
  - [ ] delete
- [ ] create/update/delete languages
  - [ ] create
  - [ ] update
  - [ ] delete
- [ ] create/update/delete shift_types
  - [ ] create
  - [ ] update
  - [ ] delete
